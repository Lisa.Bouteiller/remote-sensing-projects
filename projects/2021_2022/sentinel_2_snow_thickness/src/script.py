from skimage import io
from skimage import color
import numpy as np
import time
import matplotlib.pyplot as plt


def SurfaceGray():
    """Images without clouds"""

    start = time.time()
    image = io.imread("../data/BT/img_BT.jpg")
    image_gray = color.rgb2gray(image)
    image_gray_pixel = image_gray * 255
    image_detect = np.copy(image)

    nblignes = np.shape(image_gray_pixel)[0]
    nbcolonnes = np.shape(image_gray_pixel)[1]
    resolution = 100

    seuil = 210
    compteur = 0
    for i in range(0, nblignes):
        for j in range(0, nbcolonnes):
            if image_gray_pixel[i, j] > seuil:
                compteur += 1
                image_detect[i, j] = [0, 150, 255]

    surface = compteur * resolution
    io.imshow(image_detect)
    io.show()
    end = time.time()
    print("Temps de calcul:", (end - start))
    return ("La surface neigeuse est de", surface, "m²")


def SurfaceColor():
    start = time.time()
    image = io.imread("../data/BT/img_BT.jpg")
    image_detect = np.copy(image)

    nblignes = np.shape(image)[0]
    nbcolonnes = np.shape(image)[1]
    resolution = 100

    seuil = 210
    compteur = 0
    for i in range(0, nblignes):
        for j in range(0, nbcolonnes):
            n = 0
            for k in range(0, 3):
                if image[i, j, k] >= seuil:
                    n += 1
            if n == 3:
                compteur += 1
                for k in range(0, 3):
                    image_detect[i, j] = [0, 150, 255]

    io.imshow(image_detect)
    io.show()
    surface = compteur * resolution
    end = time.time()
    print("Temps de calcul:", (end - start))
    return ("La surface neigeuse est de", surface, "m²")


def SurfaceBTSeuil():
    start = time.time()
    image = io.imread("../data/BT/imgIR_BT.jpg")
    image_detect = np.copy(image)

    nblignes = np.shape(image)[0]
    nbcolonnes = np.shape(image)[1]
    resolution = 100

    seuilRmin = 240
    seuilRmax = 255
    seuilGmin = 40
    seuilGmax = 55
    seuilBmin = 40
    seuilBmax = 55
    seuil = [
        [seuilRmin, seuilRmax],
        [seuilGmin, seuilGmax],
        [seuilBmin, seuilBmax],
    ]
    compteur = 0
    for i in range(0, nblignes):
        for j in range(0, nbcolonnes):
            n = 0
            for k in range(0, 3):
                if image[i, j, k] >= seuil[k][0]:
                    if image[i, j, k] <= seuil[k][1]:
                        n += 1
            if n == 3:
                compteur += 1
                for k in range(0, 3):
                    image_detect[i, j] = [0, 150, 255]

    io.imshow(image_detect)
    io.show()
    surface = compteur * resolution
    end = time.time()
    print("Temps de calcul:", (end - start))
    return ("La surface neigeuse est de", surface, "m²")


def NDSIBeauTemps():
    start = time.time()
    imageB3 = io.imread("../data/BT/B3_BT.jpg")
    imageB11 = io.imread("../data/BT/B11_BT.jpg")
    grayB3 = color.rgb2gray(imageB3)
    grayB11 = color.rgb2gray(imageB11)
    image_detect = np.copy(imageB3)

    nblignes = np.shape(imageB3)[0]
    nbcolonnes = np.shape(imageB3)[1]
    resolution = 100
    compteur = 0
    for i in range(0, nblignes):
        for j in range(0, nbcolonnes):
            if grayB3[i, j] == 0:
                grayB3[i, j] = 0.004
            if grayB11[i, j] == 0:
                grayB11[i, j] = 0.004
            l = (grayB3[i, j] - grayB11[i, j]) / (grayB3[i, j] + grayB11[i, j])
            if l > 0.15:
                image_detect[i, j] = [0, 150, 255]
                compteur += 1

    io.imshow(image_detect)
    io.show()
    surface = compteur * resolution
    end = time.time()
    print("Temps de calcul:", (end - start))
    return ("La surface neigeuse est de", surface, "m²")


def SurfaceNuagesSeuil():
    """Images with clouds"""

    start = time.time()
    image = io.imread("../data/IR/img_IR.jpg")
    image_detect = np.copy(image)

    nblignes = np.shape(image)[0]
    nbcolonnes = np.shape(image)[1]
    resolution = 100

    seuilRmin = 124
    seuilRmax = 238
    seuilGmin = 117
    seuilGmax = 202
    seuilBmin = 124
    seuilBmax = 238
    seuil = [
        [seuilRmin, seuilRmax],
        [seuilGmin, seuilGmax],
        [seuilBmin, seuilBmax],
    ]
    compteur = 0
    for i in range(0, nblignes):
        for j in range(0, nbcolonnes):
            n = 0
            for k in range(0, 3):
                if image[i, j, k] >= seuil[k][0]:
                    if image[i, j, k] <= seuil[k][1]:
                        n += 1
            if n == 3:
                compteur += 1
                for k in range(0, 3):
                    image_detect[i, j] = [0, 150, 255]

    io.imshow(image_detect)
    io.show()
    surface = compteur * resolution
    end = time.time()
    print("Temps de calcul:", (end - start))
    return ("La surface neigeuse est de", surface, "m²")


def NDSINuage():
    start = time.time()
    imageB3 = io.imread("../data/IR/B3_IR.jpg")
    imageB11 = io.imread("../data/IR/B11_IR.jpg")
    grayB3 = color.rgb2gray(imageB3)
    grayB11 = color.rgb2gray(imageB11)
    image_detect = np.copy(imageB3)

    nblignes = np.shape(imageB3)[0]
    nbcolonnes = np.shape(imageB3)[1]
    resolution = 100
    compteur = 0
    for i in range(0, nblignes):
        for j in range(0, nbcolonnes):
            if grayB3[i, j] == 0:
                grayB3[i, j] = 0.004
            if grayB11[i, j] == 0:
                grayB11[i, j] = 0.004
            l = (grayB3[i, j] - grayB11[i, j]) / (grayB3[i, j] + grayB11[i, j])
            if l > 0.15:
                image_detect[i, j] = [0, 150, 255]
                compteur += 1

    io.imshow(image_detect)
    io.show()
    surface = compteur * resolution
    end = time.time()
    print("Temps de calcul:", (end - start))
    return ("La surface neigeuse est de", surface, "m²")


def Epaisseur():
    """Thickness estimation"""

    image = io.imread("../data/Epaisseur/B4_B5_B6.jpg")
    nblignes = np.shape(image)[0]
    nbcolonnes = np.shape(image)[1]
    matrice_epaisseur = np.zeros([nblignes, nbcolonnes])
    matrice_moyenne = np.zeros([nblignes, nbcolonnes])
    compteur = 0
    for i in range(0, nblignes):
        for j in range(0, nbcolonnes):
            compteur = 0
            for k in range(0, 2):
                compteur = compteur + image[i, j, k]
            matrice_moyenne[i, j] = 1 / 3 * compteur
            matrice_epaisseur[i, j] = 1 / 201 * matrice_moyenne[i, j] * 100
    plt.imshow(matrice_epaisseur, cmap="Blues_r")
    plt.colorbar()
    plt.title("snow depth (centimeters)")
    ax = plt.gca()
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    plt.show()


def route():
    """Estimation of the snow on the road"""
    imageB2 = io.imread("../data/Route/B2.jpg")
    imageB12 = io.imread("../data/Route/B12.jpg")
    grayB2 = color.rgb2gray(imageB2)
    grayB12 = color.rgb2gray(imageB12)
    image_detect = np.copy(imageB2)

    nblignes = np.shape(imageB2)[0]
    nbcolonnes = np.shape(imageB2)[1]
    resolution = 100
    compteur = 0
    L = []
    for i in range(40, nblignes - 40):
        for j in range(0, nbcolonnes):
            if grayB2[i, j] == 0:
                grayB2[i, j] = 1
            if grayB12[i, j] == 0:
                grayB12[i, j] = 1
            l = (grayB12[i, j] - grayB2[i, j]) / (grayB12[i, j] + grayB2[i, j])
            if l < 0.2:
                image_detect[i, j] = [0, 255, 15]
                L.append([i, j])
    print(L)

    image_test = io.imread("../data/Route/image_etude.jpg")
    image_route = np.copy(image_test)
    longueur = len(L)
    seuil = 210
    for coord in L:
        n = 0
        for k in range(0, 3):
            if image_test[coord[0], coord[1], k] >= seuil:
                n += 1
            if n == 3:
                for k in range(0, 3):
                    image_route[coord[0], coord[1]] = [20, 130, 255]

    io.imshow(image_detect)
    io.show()
    io.imshow(image_route)
    io.show()


def main():
    SurfaceGray()
    SurfaceColor()
    SurfaceBTSeuil()
    NDSIBeauTemps()
    SurfaceNuagesSeuil()
    NDSINuage()
    Epaisseur()
    route()


if __name__ == "__main__":
    main()
