# on utilise la biblioteque pandas pour gerer les fichiers .csv
import pandas
import matplotlib.pyplot as plt
import numpy as np


def main():
    # le fichier qu'on a récupèré sur le site de greener
    df = pandas.read_csv('../data/irradiance_avril_22.csv')
    df1 = df

    a = df['TIMESTAMP']
    b = df['Ray_Global_CMP3_Avg']

    # on cherche le caractere '12:00' pour avoir extraire les données a 12h00
    x = a[a.str.match('^.*12:00.*$') == True]
    y = b[a.str.match('^.*12:00.*$') == True]

    print(b)

    # affichage
    plt.plot(x[2:], np.array(y[2:], dtype=float))
    plt.xticks(rotation='vertical')
    plt.xlabel('Date', fontweight='bold')
    plt.ylabel('Irradiance moyennée sur une heure à midi en W/m^2', fontweight='bold')
    plt.title('Irradiance sur le toit de greenEr en avril 2022 à midi', fontweight='bold')
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
